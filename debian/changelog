python-cycler (0.12.1-1) unstable; urgency=medium

  * d/control: Adopt package. Add myself as Uploaders. Closes: #1065250.
  * d/rules: override dh_auto_clean to remove doc/build and
    doc/source/generated folder (Closes: #1046161, #1049518).
  * New upstream version
  * Testsuite: autopkgtest-pkg-python (routine-update)
  * d/control: Add Build dependency to pybuild-plugin-pyproject.
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Security-Contact.
  * Remove field Testsuite on binary package python-cycler-doc that duplicates source.
  * watch file standard 4 (routine-update)

 -- Emmanuel Arias <eamanu@debian.org>  Wed, 13 Mar 2024 18:18:27 -0300

python-cycler (0.11.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck", since debhelper now
      does this.
  * Update standards version to 4.6.2, no changes needed.

  [ Alexandre Detiste ]
  * remove old dependency on python3-six

  [ Sandro Tosi ]
  * orphan

 -- Sandro Tosi <morph@debian.org>  Sat, 02 Mar 2024 04:02:29 -0500

python-cycler (0.11.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * New upstream release
  * Use the new Debian Python Team contact name and address
  * debian/copyright
    - extend packaging copyright years
  * debian/patches
    - drop all patches, no longer needed
  * debian/{control, rules}
    - run tests via pytest, drop nose

 -- Sandro Tosi <morph@debian.org>  Wed, 03 Nov 2021 23:48:25 -0400

python-cycler (0.10.0-3) unstable; urgency=medium

  * debian/patches/doc-fix-code-input-with-no-code.patch
    - fix failure with updated ipython sphinx directive; Closes: #950145
  * Drop python2 support; Closes: #937681

 -- Sandro Tosi <morph@debian.org>  Sun, 16 Feb 2020 22:42:06 -0500

python-cycler (0.10.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * debian/control
    - mark module binary packages as Multi-Arch: foreign; Closes: #929045
    - bump Standards-Version to 4.4.1 (no changes needed)
    - wrap-and-sort
  * Build documentation with py3k sphinx
  * debian/patches/remove-mpl-only-directives.patch
    - ensure compat with new matplotlit removing only_directives sphinx ext

 -- Sandro Tosi <morph@debian.org>  Fri, 20 Dec 2019 22:48:41 -0500

python-cycler (0.10.0-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * New upstream release
  * debian/control
    - update Homepage
    - bump Standards-Version to 3.9.7 (no changes needed)
  * debian/{control, rules}
    - run tests at build time; Closes: #800908
  * Build and provide documentation; Closes: #800907

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

 -- Sandro Tosi <morph@debian.org>  Sat, 09 Apr 2016 14:03:20 +0100

python-cycler (0.9.0-1) unstable; urgency=low

  * Initial release (Closes: #791376)

 -- Sandro Tosi <morph@debian.org>  Sun, 04 Oct 2015 12:53:26 +0100
